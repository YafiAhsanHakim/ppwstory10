from django.urls import path, include
from django.conf.urls import url
from . import views
from .views import home, signup

app_name = 'homepage'

urlpatterns = [
    path('', home, name='home'), 
]