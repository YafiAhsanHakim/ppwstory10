from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from django.apps import apps
from .views import home
from django.contrib.auth.forms import UserCreationForm
from homepage.apps import HomepageConfig
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class TestUnit(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_app(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

    def test_title_exists(self):
        response = Client().get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Story 10", response_content)

    def test_form_valid(self):
        form_data = {'username': "test", 'password1' : "yafi1234", 'password2' : "yafi1234"}
        request = self.client.post('/signuppage', data = form_data)
        self.assertEqual(request.status_code, 302)
    
    def test_form_not_valid(self):
        form_data = {'username': "test", 'password1' : "yafi123", 'password2' : "yafi1234"}
        request = self.client.post('/signuppage', data = form_data)
        self.assertEqual(request.status_code, 200)

    def test_get_method(self):
        request = self.client.get('/signuppage')
        self.assertEqual(request.status_code, 200)



